# Student Agreement



<strong>Fab Academy Student Agreement</strong>
The Fab Academy is responsible for:
<ol>
<li>Teaching principles and practices of digital fabrication</li>
<li>Arranging lectures, recitations, meetings, and events for the class</li>
<li>Evaluating and providing feedback on student work</li>
<li>Offering clear standards for completing assignments</li>
<li>Certifying and archiving student progress</li>
<li>Supervising class preparation</li>
<li>Reviewing prospective students, instructors, and labs</li>
<li>Providing central staff and infrastructure for students, instructors, and labs</li>
<li>Fund-raising for costs not covered by student tuition</li>
<li>Managing and reporting on the program’s finances, results, and impacts</li>
<li>Publicizing the program</li>
<li>Promoting a respectful environment free of harassment and discrimination</li>
</ol>
I am a Fab Academy student, responsible for:

<ol>
<li>Attending class lectures and participating in reviews</li>
<li>Developing and documenting projects assigned to introduce and demonstrate skills</li>
<li>Allowing the Fab Academy to share my work (with attribution) in the class for purposes compatible with its mission</li>
<li>Honestly reporting on my work, and appropriately attributing the work of others</li>
<li>Working safely</li>
<li>Leaving workspaces in the same (or better) condition than I found them</li>
<li>Participating in the upkeep of my lab</li>
<li>Ensuring that my tuition to cover local and central class costs is covered</li>
<li>Following locally applicable health and safety guidance</li>
<li>Promoting a respectful environment free of harassment and discrimination</li></ol>
Signed by committing this file in my repository,

Mauricio Surco,
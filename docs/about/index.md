# About me

![](../images/avatar-photo.jpg)

Hi! I am Mauricio. I am work at Tecsup, in Arequipa I teach topic about Cybersecurity and Networking.

Visit this website to see my work!

## My background

I was born in a nice city called Arequipa, the city is located at the foot of a volcano.
![](../images/volcan_aqp.jpg)

## Previous work

In my previous experience, I have worked with Arduino, attempting to develop a car inspired by Batman's vehicle. Although the project was completed, I recognize the need to enhance my design skills to achieve optimal results.

Furthermore, I am proficient in various programming languages, including PHP, JavaScript, Python, and C#. These languages have allowed me to explore diverse aspects of software development and problem-solving.

Presently, I am focused on integrating Data Science principles into Networking. This exciting endeavor enables me to leverage data-driven insights to enhance network performance and efficiency.

Overall, I am passionate about continuous learning and applying my knowledge in innovative ways to contribute to meaningful projects in the tech industry.


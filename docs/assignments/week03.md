# 3. Computer Aided design

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Evaluation and selection 2D software
The main advantage that Inkscape has over Corel is that it is free, both handle similar tools, easy to understand.

## Inkscape
Inkscape is a vector graphics software used for the creation and editing of images defined by lines and curves, enabling lossless scalability. It provides drawing tools, object editing, layer management, and supports multiple file formats. It also allows for the addition of text to your designs. In this case, we will use the software to replicate an image downloaded from the internet. <br>To do this, we will first select the pen tool located in the middle-right part of the interface.

<center>
![](../images/w301.jpg){width=50%}  
Sample of how lines should be traced over the image.
</center>

Now, we proceed to create outlines around the object to be duplicated, just as shown in the following image.

<center>
![](../images/w3.02.jpg){width=50%}  
Sample of how lines should be traced over the image.
</center>

After tracing over the desired image, proceed to click on the node tool, as shown in the following image. This tool will allow you to visualize the breakpoints in the lines, which will later enable you to modify and refine the curves of the image.

<center>
![](../images/w3.03.jpg){width=50%}  
Location of the node tool.
</center>

Now, to make the curves of the lines smoother and not so rigid, click on the toolbar at the top on the "Auto-smooth selected nodes" tool. Please note that now, in each smoothed node, two controls will appear to better facilitate the smoothing of the curves. Move these controls until you achieve the desired curve.

<center>
![](../images/w3.04.jpg){width=50%}  
Location of the node smoothing tool and the achieved result.
</center>
<center>
![](../images/w3.05.jpg){width=50%}  
Controls that should be moved to achieve the desired curve.
</center>
Now, add a background color so that it can later be used for recognition by the cutter.
<center>
![](../images/w3.06.jpg){width=50%}  

</center>


## Fusion 360
In our project, we will explore Fusion 360, a versatile 3D design tool. Developed by Autodesk, Fusion 360 combines computer-aided design (CAD) and computer-aided manufacturing (CAM) capabilities. It is a popular choice for tasks like product design, engineering, and manufacturing. Fusion 360 boasts an extensive set of features, including 3D modeling, simulation, rendering, and collaborative tools, making it an all-encompassing solution for our design and engineering needs.

First, we will measure an original LEGO piece to extract its dimensions. To begin, click on the "New Sketch" icon located in the toolbar at the top.
<center>
![](../images/w3.10.jpg){width=50%}  

</center>
The following tools will be used to develop the sketch of the pieces:
Circles: This tool allows you to create circles based on the measurements taken in the previous step.
Rectangles: It will enable the generation of the rectangle that covers the circular pieces.
Trim: This tool allows you to remove excess lines.
Horizontal Vertical: Allows you to restrict the orientation point to one of the axes.
Measurement Tool: This tool allows you to measure two reference points, lines, or both.

<center>
![](../images/w3.11.jpg){width=50%}  

</center>


Now, we proceed to create the circles and rectangles while removing any excess lines using the aforementioned tools. It's important to be careful and ensure that you respect the measurements taken in the initial steps.

<center>
![](../images/w3.12.jpg){width=50%}  

</center>


Now, click on 'Finish Sketch.'

<center>
![](../images/w3.14.jpg){width=50%}  

</center>

Next, we'll transition from a 2D image to a 3D one. This process is called extrusion and can be achieved through the button marked with an arrow. You'll need to specify the extrusion parameters using the data extracted from the LEGO pieces.

<center>
![](../images/w3.15.jpg){width=50%}  

</center>

Now, your designed piece is complete
<center>
![](../images/w3.16.jpg){width=50%}  

</center>


## Files to download:

1. [Desing Toad in InkScape](../files/Toad2d.svg)
2. [Desing Piece Lego Fusion 360](../files/w2PiezaLegov9.f3d)
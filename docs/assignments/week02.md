# 2. Project management

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Assignments
## Work through a git tutorial

This week I started to getting used to the documentation process.

To start the development of the repository, we must first synchronize the space on the web with the directory on the PC, we follow these steps:

Next, we detail each step:

Git download and installation: Git’s homepage
Git Download
<ol>
<li>
<p>Git download and installation: <a href="https://git-scm.com/">Git’s homepage</a><br>
    Git Download<br></p>
    <center>
![](../images/01.jpg){width=50%}  
We go to the Homepage Git for download GUI Clients
</center>
    <center>
![](../images/02.jpg){width=50%}  
Go to the download page for our System Operate.
</center>
    <center>
![](../images/03.jpg){width=50%}  
Download Windows version.
</center>
    <center>
![](../images/04.jpg){width=50%}  
Save file to execute.
</center>
   <p></p>
<p>Git Installation
</p>
    <center>
![](../images/05.jpg){width=50%}  
Run the installation file.
</center><p></p>
</li>

<li>
<p>Repository cloning with Git<br>
    We create a folder in which the web files to be cloned will be stored, and then we run Git: 
    </p>
    <center>
![](../images/06.jpg){width=50%}  
We create and select folder to clone the repository and run GIT Bash Here  
</center>
    <center>
![](../images/07.jpg){width=50%}  
Next to run GIT Bash Here, this open the GIT commnads console. 
</center>
<p></p>
<p>We apply the following commands:
In the first one, we write our name.
In the second, we write our email address. <br>
  </p>
<pre>
  $ git config –global user.name “Your User name”
  $ git config –global user.email “Your e-mail”</pre>

<p>We create the password with RSA to manager page.</p><pre>  $ ssh-keygen -t rsa -C “Your e-mail”
</pre><center> 
![](../images/08.jpg){width=50%}  
We copy encrypted password. 
</center>


<p>We copy and paste the Public SSH key to validate the clone: 
</p><pre>  $ cat ~/.ssh/id_rsa.pub
</pre>
<center> 
![](../images/09.jpg){width=50%}  
We copy encrypted password. 
</center>
We paste encryted password in the SSH keys repository.

<center> 
![](../images/10.jpg){width=50%}  
Click the button to validate password.  
</center>

  <center> 
   
    <br>
    We paste encryted password in the SSH keys repository.<br>
 
    Click the button to validate password. 
  </center>
We get the SSH URL to start the cloning of our web space: 
  
    Copy URL with SSH

<pre>  $ git clone “URL of your page”
</pre>
  <center>
    Execute Git clone with URL SSH to clonate our web space.
  </center>
And finally we verify the copy of files: <br>
  <center>
    ![](../images/11.jpg){width=50%} <br>
    We verify the copy of files.
  </center><p></p>
  <pre>  $ git clone “URL of your page”
</pre>
</li></ol>

## Build a personal site in the class archive describing you and your final project¶
<ol>
<li>
<p>Visual Studio Code Download and installation: <a href="https://code.visualstudio.com/Download">Visual Studio Code homepage</a><br>
    Visual Studio Code download, after run the installation as manager. <br>
    </p><center>
      ![](../images/12.jpg){width=50%} <br>
</li>
<li>
<p>Edit web pages with Visual Studio Code.<br>
    We open the page to edit, select the file to edit after we choose Visual Studio Code as editor now we can see the open file and you can update it.
    </p><center>
      ![](../images/13.jpg){width=50%} <br>
    </center><p></p>
</li>
<li>
<p>Update cloud web repository with git:<br>
    This is the page before update.
      </p><center>
        ![](../images/14.jpg){width=50%} <br>
        We can see previous page.
      </center><p></p>
<p>We run Git Bash inside the cloned folder on our local drive.
  </p><center>
    ![](../images/15.jpg){width=50%} <br>
    Run Git Bash Here in our Cloned Repository Folder.
  </center><p></p>
<p>We apply the following commands to know the status of update, if you have change it show in the screen:<br>
  </p><pre>    $ git status
  </pre>
  <center>
    ![](../images/16.jpg){width=50%} <br>
    Apply “git status” to see our changes.
  </center>
  <pre>    $ git add .
  </pre>
    <center>
      ![](../images/17.jpg){width=50%} <br>
      Apply “git add .” to attach this changes.
    </center>
  <pre>    $ git commit -m “Commit’s Message”
  </pre>
    <center>
      ![](../images/18.jpg){width=50%} <br>
      Apply “git commit -m” to commission our changes.
    </center>
  <pre>    $ git push
  </pre>
  <center>
    ![](../images/19.jpg){width=50%} <br>
    Apply “git push” to update repository.
  </center>
The page refresh takes a few minutes. 
  <center>
    ![](../images/20.jpg){width=50%} <br>
    After that, with the check symbol, we can see update of the repository in the cloud.<br>
    Finally, we can see our updated page.
  </center><p></p>
</li>
</ol>
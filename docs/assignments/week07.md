# 7. Computer controlled machining

This week I worked on defining my final project idea and started to getting used to the documentation process.

First, we need to develop a model, and we'll do this using the Fusion 360 software, utilizing the tools that were explained in the previous weeks. The following image displays the finished model, where rectangular joints have been designed using circles to assist the cutting tool in making turns. The design is for a plant pot holder.


![](../images/w801.jpg){width=400}  <br>
Finished Design in Fusion 360<br>




Now, in the Fusion 360 software, we switch to the manufacturing mode to perform cutting tests and configure the tool. Tool configuration is crucial, and we must use the cutting parameters provided by the manufacturer. This manufacturing mode allows us to simulate cutting tests to anticipate any errors.


![](../images/w802.jpg){width=400}  <br>
Tool Configuration in Fusion 360<br>



![](../images/w803.jpg){width=400}  <br>
Cutting Tests<br>



![](../images/w804.jpg){width=400}  <br>
Cutting Simulation<br>




The CNC machine operates with specialized software that enables the creation of a file readable by the CNC cutter. This software allows us to measure cutting depths by selecting different views on the right side, where we can observe whether the tool penetrates the material or not.


![](../images/w805.jpg){width=400}  <br>
Side View of Cutting<br>



Now, we copy the file onto a USB drive and load it into the control module. This module allows me to:

Load the generated file.
Set the starting point for cutting.
Move the cutter.
Perform a test cut to ensure it reaches the material, among other functions.
<br>
![](../images/w8051.jpg){width=400}  <br>
Control Module of the CNC Equipment<br>



Once all cutting parameters are established, we initiate the cutting process.


![](../images/w8052.jpg){width=400}  <br>
Material Cutting Moment
![](../images/w8053.jpg){width=400}  <br>

After completing the respective cut and preparing the material, we evaluate the final finishing quality of the job.
<br>
![](../images/w806.jpg){width=400}  <br>
Final Presentation
<br>
![](../images/w807.jpg){width=400}  <br>
Final Presentation with me


## Files to download:

1. [Model in Fusion 360](../files/w8ModeloMaceterov1.f3d)
2. [Model Final in CNC Change ](../files/1001.nc)

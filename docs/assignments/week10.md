# 10. Molding and casting

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

In this project, we are going to create a silicone mold of a physical object, and then cast it with resin to obtain a copy of the original model.

Start by creating an acrylic container that is large enough to accommodate the object you want to replicate.
<div align="center">
      <img alt="" src="../images/w1001.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>

Half of this container will be covered with resin. To determine the required resin volume, use rice. Fill the container with rice.
<div align="center">
      <img alt="" src="../images/w1002.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Remove the rice and use a scale to measure the volume of resin needed to cover the image.
<div align="center">
      <img alt="" src="../images/w1003.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Before mixing the resin, cover the entire surface with a non-stick additive to prevent the silicone from sticking.
<div align="center">
      <img alt="" src="../images/w1004.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Mix the silicone with the catalyst. The catalyst will control the drying time of the silicone. Use only 3% of the catalyst in relation to the total weight of the silicone.
<div align="center">
      <img alt="" src="../images/w1005.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Pour the mixture into the container until it covers the entire image and extends 1/4 of a centimeter beyond.
<div align="center">
      <img alt="" src="../images/w1006.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
<div align="center">
      <img alt="" src="../images/w10061.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
<div align="center">
      <img alt="" src="../images/w10062.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Allow the silicone to dry, then remove the clay. Cover the other 50% with the non-stick additive, repeat the process with rice to determine the volume, mix the components (catalyst + silicone), and finally, fill the container with the prepared mixture.
<div align="center">
      <img alt="" src="../images/w1007.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
<div align="center">
      <img alt="" src="../images/w1008.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
After waiting for the necessary drying time, carefully open the silicone molds and extract the object to be copied.
<div align="center">
      <img alt="" src="../images/w1009.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Mix the catalyst with the resin and fill the mold. Perform this step carefully to ensure that all empty spaces are filled.
<div align="center">
      <img alt="" src="../images/w1010.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
<div align="center">
      <img alt="" src="../images/w1011.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
After waiting for the necessary time, and once the mold is ready, carefully open the silicone mold and extract the created object.
<div align="center">
      <img alt="" src="../images/w1012.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
<div align="center">
      <img alt="" src="../images/w1013.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
<div align="center">
      <img alt="" src="../images/w1014.jpg" width="30%"><br>
      <strong>Superficial layout of the image.</strong> <br>
</div>
Considerations:

Ensure that the object is coated with a substance that prevents the silicone molds from sticking.
Make the hole for resin filling in a way that allows all spaces to be filled.
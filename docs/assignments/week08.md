# 8. Embedded Programming

## What is ESP32?
ESP32: The ESP32 is a low-cost, low-power microcontroller designed by Espressif Systems. It stands out for its Wi-Fi and Bluetooth connectivity, dual-core architecture, extensive range of interfaces, and low-power options. Widely employed in IoT projects and embedded applications.
### Main Features
1.	Dual-Core Microcontroller: The ESP32 incorporates a dual-core Xtensa LX6 microcontroller, allowing for parallel task execution and overall improved performance.
2.	Wireless Connectivity: It provides 802.11 b/g/n Wi-Fi and Bluetooth 4.2 connectivity, making it ideal for IoT applications and wireless communication projects.
3.	Low-Power Processor: Despite its processing power, the ESP32 is designed to be energy-efficient, making it suitable for battery-powered applications.
4.	Ports and Peripherals: The board features various ports and peripherals, including GPIO (General Purpose Input/Output) ports, UART, SPI, I2C, ADC, DAC, PWM, among others.
5.	Flash Memory and RAM: It has a significant amount of flash memory for storing programs and data, as well as an adequate amount of RAM for running complex applications.
6.	Security: The ESP32 includes security features such as cryptographic support, Secure Boot, and Flash Encryption to protect the system's integrity.
7.	Integrated Antennas: It comes with integrated antennas for Wi-Fi and Bluetooth, eliminating the need for external components for wireless connectivity.
8.	Development Support: It supports the Arduino development platform and the Espressif IDF (IoT Development Framework), providing libraries and tools for programming the board.
9.	Versatility of Applications: Due to its power and versatility, the ESP32 is used in a wide range of applications, from IoT projects to embedded systems and rapid prototypes.
10.	Affordable Cost: Despite its advanced features, the ESP32 is relatively affordable, making it a popular choice for DIY projects and commercial developments.<br>

![](../images/Electro/ESP32-Pinout-1.jpg)<br>
ESP-WROOM 32 Pinout




## What is Arduino Mega?
Arduino Mega: The Arduino Mega utilizes the ATmega2560 microcontroller with 256 KB Flash memory, 8 KB RAM, and 4 KB EEPROM. It provides numerous digital and analog inputs/outputs (I/O) and peripherals, making it well-suited for larger and more intricate projects in the field of electronics.
### Main Features
1.	Microcontroller: Arduino Mega is powered by the ATmega2560 microcontroller, providing a large number of digital and analog input/output pins.
2.	Digital I/O Pins: It features 54 digital input/output pins, allowing for versatile connectivity and control of digital devices.
3.	Analog Inputs: With 16 analog inputs, Arduino Mega supports the measurement of analog signals, enabling precise sensing for various applications.
4.	Flash Memory: The board comes equipped with 256 KB of Flash memory, providing sufficient space for storing program code and data.
5.	RAM: Arduino Mega has 8 KB of SRAM, facilitating the execution of complex tasks and handling larger datasets.
6.	Clock Speed: It operates at a clock speed of 16 MHz, offering a balance between processing power and energy efficiency.
7.	UART, SPI, I2C Communication: Arduino Mega supports multiple communication protocols, including UART (Universal Asynchronous Receiver-Transmitter), SPI (Serial Peripheral Interface), and I2C (Inter-Integrated Circuit), enhancing its compatibility with various devices.
8.	USB Connectivity: The board includes a USB interface for programming and communication with a computer, making it convenient for development and debugging.
9.	Power Supply: Arduino Mega can be powered via a USB connection or an external power supply, providing flexibility in different usage scenarios.
10.	Compatibility: It is compatible with a wide range of shields and expansion modules, allowing users to extend its capabilities and tailor it to specific project requirements.
11.	Open-Source: Arduino Mega, like other Arduino boards, is open-source hardware and software, encouraging community collaboration and innovation.
12.	Versatility: Arduino Mega is suitable for a variety of projects, particularly those that require a larger number of I/O pins, extensive connectivity, and more computational power.<br>

![](../images/Electro/Arduino-Mega-Board-Layout.jpg)<br>
Arduino Mega Pinout Pinout

## Comparison between ESP32 and Arduino Mega?
1.	Microcontroller:
•	ESP32: Based on a 32-bit dual-core Xtensa processor.
•	Arduino Mega: Uses the 8-bit ATmega2560 AVR microcontroller.
2.	Wireless Connectivity:
•	ESP32: Integrated Wi-Fi and Bluetooth.
•	Arduino Mega: No built-in wireless connectivity (can be added with external modules).
3.	Memory:
•	ESP32: Flash for storage and RAM for execution.
•	Arduino Mega: 256 KB Flash, 8 KB RAM, and 4 KB EEPROM.
4.	I/O and Peripherals:
•	ESP32: Wide variety of I/O interfaces, camera support, etc.
•	Arduino Mega: Numerous digital and analog I/O, standard interfaces (UART, I2C, SPI).
5.	Software Development:
•	ESP32: Commonly programmed using the Arduino development environment.
•	Arduino Mega: Developed using the Arduino IDE.
6.	Applications:
•	ESP32: Ideal for IoT projects, wireless applications, and embedded systems.
•	Arduino Mega: Suited for larger projects with numerous I/O requirements.
7.	Size and Power Consumption:
•	ESP32: Generally smaller and can be more power-efficient.
•	Arduino Mega: Larger in size and may have higher power consumption compared to the ESP32.



# Previous configurations before programming
## Step 1: Installing or Updating the Arduino IDE
The first step in installing the ESP32 Arduino core is to have the latest version of the Arduino IDE installed on your computer. To do this, follow the steps in the following image.<br>
![](../images/Electro/arduinoIDE.jpg)<br>
Screenshot of the Arduino IDE

Step 2: Installing the USB-to-Serial Bridge Driver
There are numerous ESP32-based development boards available. Depending on the design, you may need to install additional drivers for your USB-to-serial converter before you are able to upload code to your ESP32.
For example, the ESP32 DevKit V1 uses the CP2102 to convert USB signals to UART signals, whereas the WeMos ESP32 Lite uses the CH340G. The ESP32-CAM, on the other hand, lacks an onboard USB-to-serial converter and requires a separate module.
 
First, access the Device Manager. Within it, check if the new device displays a warning icon. If no icon is shown, as in the following image, then the device has been recognized.<br>
![](../images/Electro/adminDispositivo.JPG)<br>
Screenshot of the Arduino IDE

If the device is not recognized, download and extract the driver from the following page. Then, right-click on the image and select the "Update driver" option.<br>
![](../images/Electro/actualizaControlador.JPG)<br>
Screenshot of the Arduino IDE

Now, choose "Browse my computer for driver software" in the next window.<br>
![](../images/Electro/actualizaControlador1.JPG)<br>
Screenshot of the Arduino IDE

In the following image, select the browser and navigate to the folder where the driver was downloaded.<br>
![](../images/Electro/actualizaControlador2.JPG)<br>
Screenshot of the Arduino IDE
Wait a few minutes, and it should display an image like the one below, indicating that the device has been found.<br>
![](../images/Electro/actualizaControlador3.JPG)<br>
Screenshot of the Arduino IDE
Make sure to inspect your board carefully to identify the USB-to-serial converter that is present. You’ll probably have either CP2102 or CH340 populated on the board.

## Step 3: Installing the ESP32 Arduino Core
Launch the Arduino IDE and navigate to File > Preferences.
 
Fill in the “Additional Board Manager URLs” field with the following.
https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json
Then, click the “OK” button.
 
If you have already entered the URL for the ESP8266 boards or any other board, you can click on the icon to the right of the field to open a window where you can add additional URLs, one for each row.
 
Now navigate to Tools > Board > Boards Manager…<br>
![](../images/Electro/ESP1.JPG)<br>
Screenshot of the Arduino IDE
Filter your search by entering ‘esp32‘. Look for ESP32 by Espressif Systems. Click on that entry, and then choose Install.<br>
![](../images/Electro/ESP2.JPG)<br>
Screenshot of the Arduino IDE<br>
![](../images/Electro/ESP3.JPG)<br>
Screenshot of the Arduino IDE
## Step 4: Selecting the Board and Port
After installing the ESP32 Arduino Core, restart your Arduino IDE and navigate to Tools > Board to ensure you have ESP32 boards available.
 
Now select your board in the Tools > Board menu (in our case, it’s the DOIT ESP32 DEVKIT V1). If you are unsure which board you have, select ESP32 Dev Module.<br>
![](../images/Electro/ESP4.JPG)<br>
Screenshot of the Arduino IDE
Finally, connect the ESP32 board to your computer and select the Port.
 

That’s it! You can now begin writing code for your ESP32 in the Arduino IDE.
You should make sure you always have the most recent version of the ESP32 Arduino core installed.
Simply navigate to Tools > Board > Boards Manager, search for ESP32, and verify the version you have installed. If a newer version is available, you should install it.


## Step 5: Testing the Installation
Once you’ve finished the preceding steps, you are ready to test your first program with your ESP32! Launch the Arduino IDE. If you disconnected your board, plug it back in.

Let's develop a program where pressing the 'a' key turns on the LED, and pressing the 'b' key turns it off. To achieve this, we'll use conditionals, and for output, we'll utilize the serial print module to display the output. The code used is as follows:

```
char valor;

void setup() {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if (Serial.available()){
    valor =Serial.read();
    Serial.println(valor);
    if (valor=='a'){
      digitalWrite(13,HIGH);
      Serial.println("Encendido");
    }
    if (valor=='b'){
      digitalWrite(13,LOW);
      Serial.println("Apagado");
    }
  }
}
```

The output shown with the code is the following:<br>

![](../images/Electro/ESP6.JPG){width=50%}<br>
Screenshot of the Arduino IDE

## Code explanation
Variable Declaration:

...
char valor;
...
A character variable named valor is declared. This variable will be used to store the characters received through serial communication.
Setup Function:

...
void setup() {
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}
...
In the setup function:
pinMode(13, OUTPUT); configures pin 13 as an output for an LED. This pin will control the LED.
Serial.begin(9600); initializes serial communication with a baud rate of 9600, enabling communication with an external device (like a computer) via the Serial Monitor.
Loop Function:

...
void loop() {
  if (Serial.available()) {
    valor = Serial.read();
    Serial.println(valor);
    if (valor == 'a') {
      digitalWrite(13, HIGH);
      Serial.println("Encendido");
    }
    if (valor == 'b') {
      digitalWrite(13, LOW);
      Serial.println("Apagado");
    }
  }
}
...
In the loop function:
if (Serial.available()): Checks if there is data available in the serial buffer.
valor = Serial.read();: Reads a character from the serial buffer and stores it in the variable valor.
Serial.println(valor);: Prints the received character to the Serial Monitor.
Two conditional statements:
if (valor == 'a'): If the received character is 'a', it turns on the LED (connected to pin 13) and prints "Encendido" to the Serial Monitor.
if (valor == 'b'): If the received character is 'b', it turns off the LED and prints "Apagado" to the Serial Monitor.
Overall Functionality:

The Arduino continuously checks for incoming characters through serial communication.
Depending on the received character ('a' or 'b'), it performs corresponding actions: turning on/off the LED and providing feedback through the Serial Monitor.

## Download Code

- [Codigo](../files/Codigo1_ESP32.txt)


# 1. Principles and practices

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

During my online research, I discovered that Information Technology is one of the most in-demand professions worldwide. To encourage more students to pursue this field, implementing various strategies is crucial. Personally, I find experimentation and educational games to be highly intriguing approaches.

While there are several enterprises offering such educational games in the marketplace, the cost can be prohibitive. As a result, I am eager to develop an affordable educational game that provides a hands-on experience with technology.

My goal is to make technology education accessible to a wider audience, offering an engaging and cost-effective alternative for students interested in the IT industry. By creating an interactive and immersive learning experience, I hope to inspire the next generation of IT professionals to explore this dynamic and ever-evolving field. Together, we can pave the way for a brighter and more tech-savvy future.


## Purpose of the final project

Creating a modular Boxing bot for personalized and easy handling.
![](../images/robotBoxin.jpeg)


## What will you do?
Creating a LEGO-like assembly boxer robot controlled by an Android device.

## Who will use it?
Intended for schoolchildren aged 8 and above, this project aims to introduce them to the world of computing.